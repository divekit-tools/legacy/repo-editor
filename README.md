# Repo Editor

The Repo Editor is a tool for automated editing of GitLab repositories. It allows making changes to multiple projects simultaneously, based on preconfigured assets and settings.

## Quick start

1. **Prepare Configuration:**
   - Copy `.env.example` to `.env` and fill in the required values:
     ```
     API_TOKEN=YOUR_GITLAB_API_TOKEN
     HOST=https://your-gitlab-instance.com
     DIVEKIT_MAINBRANCH_NAME=main
     ```
   - Adjust `src/main/config/editorConfig.json` to your needs.

2. **Prepare Assets:**
   - Place the files to be changed in the `assets/input` directory (or in a custom directory).
   - Structure the assets in subdirectories: `all`, `code`, or `test`.

3. **Install Dependencies:**
   ```
   npm install
   ```

4. **Run Repo Editor:**
   ```
   npm start
   ```
   Or, to use the ARS folders:
   ```
   npm run useSetupInput
   ```

5. **Check Logs:**
   - The Repo Editor logs its actions. Check the output for successes or errors.


## How it works
When you run `npm start`, the Repo Editor performs the following steps:

1. Loads the configuration from the `.env` file and `editorConfig.json`.
2. Validates the configuration.
3. Updates the assets based on the configured path prefix.
4. Retrieves all configured projects from GitLab.
5. Applies the changes to the selected projects by creating or updating files.

### Detailed Functionality

1. **editorConfig.json:**
   The configuration file contains important settings:
   ```json
   {
     "onlyUpdateTestProjects": true,
     "onlyUpdateCodeProjects": false,
     "groupIds": [404],
     "logLevel": "info",
     "commitMsg": "Patch transfer evaluation.md from M2 with [skip ci] on 2023-06-07, 17:04"
   }
   ```
   - `onlyUpdateTestProjects` and `onlyUpdateCodeProjects` control which project types are updated.
   - `groupIds` defines the GitLab groups in which projects are edited.
   - `logLevel` sets the logging level.
   - `commitMsg` is the message for all commits.

2. **Configuration Validation:**
   - Checks the `.env` file for errors.
   - Ensures that both flags `onlyUpdateCodeProjects` and `onlyUpdateTestProjects` are not active simultaneously.

3. **Asset Update:**
   - Searches for files in the configured path (default: `assets/input`).
   - Creates `Asset` objects for each file found.

4. **Project Configuration:**
   - Projects are configured based on the `groupIds`.
   - Important values: Project ID, Project Name, Main Branch Name.

5. **Change Execution:**
   - Iterates over all configured projects.
   - Filters assets based on project specificity.
   - Attempts to create new files or update existing ones.

6. **File Changes:**
   - Creates commit actions (CREATE or UPDATE) for each asset.
   - Reads the content of the local file and encodes it in Base64.
   - Sends API calls to GitLab for file creation or update.
   - If errors occur during creation, an update is automatically attempted.

## Notes

- Ensure your GitLab API token has the necessary permissions.
- Be cautious when using, as the Repo Editor can make changes to multiple repositories.
- Test the Repo Editor first with a small number of projects before applying it to many repositories.
- The Repo Editor uses the GitLab API for all operations.
- Assets can be project-specific or general, which is determined by the directory structure.
- Changes are made directly in the main branch of the repositories.
- Errors are logged, but the process continues for the remaining projects/assets.

For more information, refer to the [Divekit documentation](https://divekit.github.io/docs/repo-editor/).
